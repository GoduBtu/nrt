package godz.wallpaperchanger

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }



    fun onClickButton(view: View) {
        val intent = Intent(this, SetWallpaperPage::class.java)
        when(view)
        {
            blurryLightsImageButt -> {intent.putExtra("Word","blurryLights")}
            sunRiseImageButt -> {intent.putExtra("Word","sunrise")}
            moonImageButt -> {intent.putExtra("Word","moonPic")}
            mountainImageButt ->{intent.putExtra("Word","mountain")}
            colorfulStuffImageButt -> {intent.putExtra("Word","colorful")}
            purpleMirrorImageButt -> {intent.putExtra("Word","purpleMirror")}
            digitalDotsImageButt -> {intent.putExtra("Word","digitalDots")}
            lightingHouseImageButt -> {intent.putExtra("Word","lightingHouse")}
            manOnLakeImageButt -> {intent.putExtra("Word","manOnLake")}
            manOnMoonImageButt -> {intent.putExtra("Word","manOnMoon")}
            moonWayImageButt -> {intent.putExtra("Word","moonWay")}
            illusionImageButt -> {intent.putExtra("Word","illusion")}
            oceanImageButt -> {intent.putExtra("Word","ocean")}
            parisMicroImageButt -> {intent.putExtra("Word","parisMacro")}
            starsAndStuffImageButt -> {intent.putExtra("Word","starsAndStuff")}
            turtleBWImageButt -> {intent.putExtra("Word","turtleBW")}
            milkywayImageButt -> {intent.putExtra("Word","milkyway")}
            digitalMountainImageButt -> {intent.putExtra("Word","digitalMountain")}

        }
        startActivity(intent)
    }
}
