package godz.wallpaperchanger

import android.app.WallpaperManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_set_wallpaper_page.*
import java.io.IOException

class SetWallpaperPage : AppCompatActivity() {

    var word= ""
    var drawId=R.drawable.sunraise
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_wallpaper_page)

        word = intent.getStringExtra("Word")
        when(word)
        {
            "blurryLights" -> drawId = R.drawable.blurry_lights_wallpaper
            "sunrise" -> drawId = R.drawable.sunraise
            "moonPic" -> drawId = R.drawable.moon_wallpaper
            "mountain" -> drawId = R.drawable.mountain_wallpaper
            "colorful" -> drawId = R.drawable.colorful_stuff
            "purpleMirror" -> drawId = R.drawable.purple_mirror
            "digitalDots" -> drawId = R.drawable.digital_dots
            "lightingHouse" -> drawId = R.drawable.lighting_house
            "manOnLake" -> drawId = R.drawable.man_on_lake
            "manOnMoon" -> drawId = R.drawable.man_on_moon
            "moonWay" -> drawId = R.drawable.moon_way
            "illusion" -> drawId = R.drawable.illusion
            "ocean" -> drawId = R.drawable.ocean
            "parisMacro" -> drawId = R.drawable.paris_micro
            "starsAndStuff" -> drawId = R.drawable.stars_and_stuff
            "turtleBW" -> drawId = R.drawable.turtle_bw
            "milkyway" -> drawId =R.drawable.milkyway
            "digitalMountain" -> drawId = R.drawable.digital_mountain
        }
        image.setImageResource(drawId)
    }

    fun onClickSetButton(view: View) {

        val myWallpaperManager: WallpaperManager = WallpaperManager.getInstance(applicationContext)
        try {
            myWallpaperManager.setResource(+ drawId)
        } catch (e: IOException) {
            e.printStackTrace()
        }


    }
}
